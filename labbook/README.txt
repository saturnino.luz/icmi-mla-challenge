0. Software requirments:
========================

R: http://cran.r-project.org/

RWeka package (if you intend to use the weka classfiers): http://cran.r-project.org/web/packages/RWeka/index.html

ROCR: http://rocr.bioinf.mpi-sb.mpg.de/

1. Data preparation:
====================

a) Created vocalisation labels using audacity+nyquist sound finder
   plugin. Parameter setting:

Threshold: -36dB
Min. silence duration: 1s
Voc begin offset: 0.1s
Voc end offset: 0.2s

Brief description of algorithm:

1. convert stereo tracks to mono
2. resamples to a low sample rate (~100 samples per second).  
3. For each sample
    if sample level above Threshold
       find time value of sample
       store time in list
    else
       convert list to timestamps and create voc label

Labels stored in ../data/GxDy.csv (where x \in {1,...,6} and y \in
{1,2}). 

b) Stored coding data (metadata, containing start and end time for
   each ) in the same directory; i.e. 
   ../data/GxDy_time_boundaries.csv


c) created functions to load vocalisation data sets 
   (../R/expertdet.R:makeRawVocDataSet() ) and coding data sets
   (../R/expertdet.R:makeProblemTimeScoresDataSet() )

   E.g.:

   v <- makeRawVocDataSet()   
   p <- makeProblemTimeScoresDataSet()

   With respect to answer correctness, the dataset is fairly
   imbalanced: 19.6% incorrect vs 80.4% correct

   prop.table(table(p$correct))

       FALSE      TRUE 
   0.1957672 0.8042328 

d) Filtered out small vocalisations (in the majority of cases 
   breathing noises and other non-speech sounds) from raw voc data
   set. Used  filterOutSmallVocs(rawvocdf, minvocsize=1) to eliminate
   vocs shorter than 1s. 

   N.B: that these short noises are indeed noisy random  data can be
   seen by comparing kNN classification of problem solving dialogues
   into those that resulted in correct and those that produced
   incorrect answers. With the raw dataset (including short
   vocalisations), produced by 
    
   printPerformanceARFFfile(vocdf=makeRawVocDataSet(),
       file='../data/correctness/all-raw-voclevel.arff', individual=F, anonymous=T)
   
   We get just over 16% recall (for incorrect answers; a minority
   class) and 13% precision; These figures are worse than 'informed' random
   baseline performance (recall ~19%; precision 16%; see below).

   However, with the filtered data set

   printPerformanceARFFfile(vocdf=filterOutSmallVocs(makeRawVocDataSet()),file='../data/correctness/all-filter-voclevel.arff', individual=F, anonymous=T)

   performance is significantly better than baseline: recall = 43.2 %
   and precision = 39% (AUC, ROC ~ 70%; just about acceptable as a
   diagnostic test).

e) Created ARFF files for correctness experiments with
   printPerformanceARFFfile() as shown above

e.1) Each instance of the ARFF file is a 'flattened' vocalisation
     graph. Vocalisation graphs in matrix format (ttpm, in fact) can
     be obtained as shown:

   z <- getSampledVocalMatrix(getProblemVocTable('G1D1', '1A')$speaker)

   Alternatively, for aggregate vocalisation matrices, use:

   z <- getSampledVocalMatrix(identifyVocalisations(as.character(getProblemVocTable('G1D1', '1A')$speaker)))

e.2) Graphs can be generated in DOT notation (code in markov.R)
     through, e.g.:

   printDotFile(getAnonymousProbMatrix(z), file='../data/graphs/G1D1-1A-individual.dot', rankdir='TB')

    ('TB' gives a top-to-bottom layout)

   See also igraph functions for interactive visualisation.

f) Created expert dataset with 
   
   expdf  <- getExpertLeaderDataSet()

   consisting of the following features:

    session: session ID, for ref purposes 
             only (not part of item representation)
    pid: problem ID, also for ref purposes only 
         (not part of item representation)
    speaker: speaker ID, ditto
                   
    vocsum: sum of voc durations
    vocmean: mean duration
    vocsd: voc standard dev
    pSgF: p(speaker|floor)
    pFgS: p(floor|speaker) ; prob of a transition from s to floor
    pSgG: p(speaker|groupvoc)
    pGgS: p(groupvoc|speaker)
    entS: entropy of p(s_i|speaker)
    expert: dominant expert (Boolean)
    leader: session leader?

g) Generated an ARFF version of the expert dataset, for later
   reference:

   write.arff(expdf, file='../data/domspeaker/fulldataset.arff')


2. PROCESSING
==============

2.1 Correctness prediction
--------------------------

1) Baseline analysis. A Trivial acceptor (given that 'correct' is the
   majority class would give 80% precision for TRUE, and
   precision=recall=0 for FALSE. A simulated classifier that sampled
   answers according to category generalities proportional to the generalities
   observed in the actual dataset would give recall ~ 19% and
   precision ~ 17% for the 'incorrect' category:

   mean(sapply(1:10, function(X)recall(sample(c(F,T), 189, replace=T, 
        prob=c(0.1957672, 0.8042328)), p$correct, F)))
   [1] 0.1891892

   mean(sapply(1:10, function(X)precision(sample(c(F,T), 189,
   replace=T, prob=c(0.1957672, 0.8042328)), p$correct, F)))
   [1] 0.1660488
  
2) Correctness prediction using Vocalisation-level graphs

  We employed k-NN classification based on graph similarity. Since
  there are relatively few errors, and some sessions have in fact no
  errors at all (

   round( tapply(p$correct,p$session,mean), digits=2)

   G1D1 G1D2 G2D1 G2D2 G3D1 G3D2 G4D1 G4D2 G5D1 G5D2 G6D1 G6D2 
   0.81 0.88 0.38 0.50 0.93 1.00 0.62 0.88 0.94 1.00 0.87 0.88 

  ) we generalised over the entire data set, rather than try to make
  predictions specific to individual group sessions. This exercise can
  therefore be seen as an attempt to find out if vocalisation patterns
  in group dialogues can provide a general indication as to whether
  the group interaction resulted in a correct answer or not.

  Using vocalisation level graphs and a simple Euclidean distance
  metric for 1-NN graph matching, 10-fold x-validation yielded:

  r <- runExperiment('../data/correctness/all-filter-voclevel.arff',n=10)
  
  printResultsSummary(r, 1)

  Summary for experiment "Correctness prediction Experiment
  Avg over 10-fold evaluation for "runExperiment("../data/correctness/all-filter-voclevel.arff",     n = 10)"
  on classifierclassifier and threshold 0.
  =====================================

  TRUE                     FALSE
  -------------------      ----------------------
  Accuracy:   0.824         Accuracy:  0.405
  Precision:  0.851         Precision: 0.357 
  Recall:     0.824         Recall:    0.405 
  F_1:        0.837         F_1:       0.380
  Precision:  0.827         Precision: 0.333       (macro-averaged)
  Recall:     0.804         Recall:    0.286       (macro-averaged)
  F_1:        0.808         F_1:       0.300       (macro-averaged)


  Curiously, Weka gives slightly better results, even though in theory
  it uses the same nearest-neighbour algorithm:

  === Detailed Accuracy By Class ===

               TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
                 0.836     0.568      0.858     0.836     0.847      0.652    TRUE
                 0.432     0.164      0.39      0.432     0.41       0.652    FALSE
Weighted Avg.    0.757     0.489      0.767     0.757     0.761      0.652

  === Confusion Matrix ===

   a   b   <-- classified as
 127  25 |   a = TRUE
  21  16 |   b = FALSE

  Sticking with R, using a 3-nn model weighted by inverse
  Euclidean distance (so as to generate more diverse probability
  estimates), and setting cross validation to 12 (so that for each
  session we test in one session and train on the remaining 11 [NB:
  unfortunately this isn't strictly true, since 1A and 1B are missing
  from G3D1]) we can assess prediction effectiveness in terms of its
  ROC curve:

  > r <- runExperiment(x,n=12, controlpars=alist(I=T, K=3), debug=-1)
    data frame passed as argument (Target: correct; Generality: 80.5%)

  > printResultsSummary(r,1);

Summary for experiment "Correctness prediction Experiment
Avg over 12-fold evaluation for "runExperiment(x, n = 12, controlpars = alist(I = T, K = 3), debug = -1)"
on classifierclassifier and threshold 0.
=====================================

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.861         Accuracy:  0.361
Precision:  0.844         Precision: 0.394 
Recall:     0.861         Recall:    0.361 
F_1:        0.852         F_1:       0.377
Precision:  0.833         Precision: 0.491       (macro-averaged)
Recall:     0.856         Recall:    0.394       (macro-averaged)
F_1:        0.832         F_1:       0.373       (macro-averaged)


  These results represent a small improvement over the 1-nn model,
  specially if averaged over individual sessions
  (macro-averaged). Micro averaged results, however, are probably less
  accurate, since generality [i.e. correct:#ofproblems ratio) varies
  widely among the different sessions. In any case, the area under the
  ROC curve for this classifier is ~ 71% (not great, but 'fairly
  accurate'). See expertdet.R:plotROCCurve() for a wrapper for the
  code below):

> PR <- prediction(unlist(r$prob), unlist(r$ref)); plot(performance(PR, measure="tpr", x.measure="fpr")); mtext( paste('AUC = ', performance(PR, measure="auc")@y.values[[1]])) 

or (using the convenience function provided in expertdet.R):

> plotROCCurve(r, auclabel='AUC (3-nn) =')

  The ROC curve plot is shown in ROC-correctness-full-12-fold-3nn.pdf 

> dev.copy2pdf(file='../labbook/ROC-correctness-full-12-fold-3nn.pdf', paper='special', family='Helvetica')

  Removing the two Group-5 sessions ('G5D2' and 'G5D1'), one of which contains no errors,
  and running again one fold per session (10-fol cross validation)
  improves the macro averages. This was expected, given that two very imbalanced
  folds were removed.

> r <- runExperiment(x[!p$session%in%c('G5D2', 'G5D1'),],n=10, controlpars=alist(I=T, K=3), debug=-1)
data frame passed as argument (Target: correct; Generality: 77.2%)
> printResultsSummary(r,1)

Summary for experiment "Correctness prediction Experiment
Avg over 10-fold evaluation for "runExperiment(x[!p$session %in% c("G5D2", "G5D1"), ], n = 10,     controlpars = alist(I = T, K = 3), debug = -1)"
on classifierclassifier and threshold 0.
=====================================

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.825         Accuracy:  0.361
Precision:  0.803         Precision: 0.394 
Recall:     0.825         Recall:    0.361 
F_1:        0.814         F_1:       0.377
Precision:  0.791         Precision: 0.560       (macro-averaged)
Recall:     0.826         Recall:    0.433       (macro-averaged)
F_1:        0.791         F_1:       0.408       (macro-averaged)

  However, the simulated baseline in this case is also higher (~ 23%
  for precision and recall):

X <- mcSimulateCorrectnessClass(p[!p$session %in% c("G5D2", "G5D1"),])
MC simulation (1000runs)
--------------------
 - Precision: 0.228184
 - Recall:    0.2266111




  The results reported here, although fairly weak in terms of diagnostic power, are
  a substantial improvement over the informed, Monte Carlo baseline (1). 


2.2 Expert prediction:
----------------------

For this experiment we used the dataset described in section 1.f,
above. We have interpreted this task is somewhat differently from
Oviatt et al. (2013). The benchmark reported by Oviatt et al. is
assessed with respect to a cummulative prediction as problem solving
sessions (the solutions contributted, more specifically) are
presented. That is, the prediction algorithm used was simply the
following: given n problem solving sessions, select the student who
contributed the most solutions (or the most correct solutions) as the
dominant expert. This simple algorithm is shown to predict the
dominant expert 100% accurately after only n=7 problems (n=12 for
correctly contributed solutions). However, since the number of correct
solutions forms part of the definition of expertise according to the
annotation schema, and the accuracy of solutions is directly
proportional to the overall number of solutions contributed
(i.e. participants are more likely to contribute correct answers than
incorrect ones), classification based on these features is not very
informative. 

Since for each problem solving activity there is a single participant
identified as the dominant expert, classifiation can be performed in a
Bayesian framework by taking the maximum from  a set  of 
instances, each describing a participant. 
The goal is to predict which instance within this set of
instances corresponds to the target (dominant expert).

For a representation <InstID, (representation), target?>
one could have, for example:

  InstanceSet = {<s1, (repres_s1...), FALSE>,
                 <s2, (repres_s2...), TRUE>,
                 <s3, (repres_s3...), FALSE>}

The function classifyByGroups() will return the instance that is most likely to
be the target, that is, 

    s* =  \arg\max_{s=1}^3 P(s,repres_s|target)P(target)

where the conditional is simplified according to the conditional
indepence assumption in the usual way.

We ran experiment with different sizes of test sets (with respect to
problem solving sessions, pid, *not* speaker instances) per session,
starting from 15 (3 training instances, 1 pid  per session) down to 9
(21 instances, 7 pid per session. runExpertExperimentSeries() provides
a handy wrapper for it:

 x <- generateExpertLeaderDataSet()
 s <- runExpertExperimentSeries(x, 15:9, debug=-1)

List s stores the results per session: 

pidA     = accuracy by problem (general accuracy; voting by pid)
pidSD    = standard dev. for the above

sessionA = accuracy by session (voting among all speaker instances in
           a session

For the setting above we obtained:

s
$trainsize
[1] 1 2 3 4 5 6 7

$pidA
[1] 0.5388889 0.5833333 0.5512821 0.6041667 0.6060606 0.6250000 0.7037037

$pidAsd
[1] 0.2173455 0.2669718 0.2912261 0.1816208 0.1790422 0.1356801 0.1663858

$sessionA
[1] 0.6666667 0.7500000 0.6666667 0.8333333 0.7500000 1.0000000 0.9166667


where trainsize is given in terms of problem solving session, so
for trainsize 1 the algorithm 'sees' 3 (speaker) instances, and so on. 
